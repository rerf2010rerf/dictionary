package nsu.javatech.dict;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TranslatorImpl.class)
public class TranslatorImplTest {

  private static final String DATABASE_CONFIG_PARAM = "database";

  private DictDao dao;

  private TranslatorImpl translator;

  @Before
  public void initTests() throws Exception {
    final Vertx vertx = mock(Vertx.class);
    final JsonObject config = new JsonObject().put(DATABASE_CONFIG_PARAM, new JsonObject());
    dao = mock(DictDao.class);

    PowerMockito.whenNew(DictDao.class)
      .withArguments(same(vertx), same(config.getValue(DATABASE_CONFIG_PARAM)))
      .thenReturn(dao);

    translator = new TranslatorImpl(vertx, config);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void translate_returnsDictDaoResult() {
    final String testWord = "word";
    final Future<DictEntry> result = mock(Future.class);

    translator.translate(testWord, result);

    verify(dao).fetchEntry(eq(testWord), same(result));
  }
}