package nsu.javatech.dict;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest(JDBCClient.class)
@SuppressWarnings("unchecked")
public class DictDaoTest {

  private static final String TRANSLATION_WORD_COLUMN = "translation_word";
  private static final String COMMENT_WORD_COLUMN = "comment_word";
  private static final String CLASS_NAME_COLUMN = "class_name";

  private static final String SELECT_SQL =
    "select cl.class_name as " + CLASS_NAME_COLUMN +
      ", t.word as " + TRANSLATION_WORD_COLUMN +
      ", tr.translate_comment as " + COMMENT_WORD_COLUMN +
      " from word_classes cl, source_words s, target_words t, translates tr" +
      " where s.word = ?" +
      " and s.word_id = tr.word_id" +
      " and tr.translate_id = t.word_id" +
      " and t.word_class = cl.class_id";
  private static final String WORD = "word";
  private static final String TRANSLATION = "translation";
  private static final String COMMENT = "comment";
  private static final String CLASS = "class";

  private AsyncResult connResult;
  private AsyncResult queryResult;


  private DictDao dao;

  @Before
  public void initTests() {
    final JDBCClient client = mock(JDBCClient.class);
    final SQLConnection conn = mock(SQLConnection.class);

    PowerMockito.mockStatic(JDBCClient.class);
    when(JDBCClient.createShared(any(), any())).thenReturn(client);
    dao = new DictDao(null, new JsonObject());

    connResult = mock(AsyncResult.class);
    when(connResult.succeeded()).thenReturn(true);
    when(connResult.result()).thenReturn(conn);
    when(client.getConnection(argThat(new ArgumentMatcher<Handler<AsyncResult<SQLConnection>>>() {
      @Override
      public boolean matches(Object o) {
        ((Handler<AsyncResult<SQLConnection>>) o).handle(connResult);
        return true;
      }
    }))).thenReturn(client);

    final ResultSet resultSet = mock(ResultSet.class);
    queryResult = mock(AsyncResult.class);
    when(queryResult.succeeded()).thenReturn(true);
    when(queryResult.result()).thenReturn(resultSet);

    when(resultSet.getRows()).thenReturn(Collections.singletonList(
      new JsonObject().put(TRANSLATION_WORD_COLUMN, TRANSLATION)
        .put(COMMENT_WORD_COLUMN, COMMENT)
        .put(CLASS_NAME_COLUMN, CLASS))
    );

    when(conn.queryWithParams(eq(SELECT_SQL), eq(new JsonArray().add(WORD)),
      argThat(new ArgumentMatcher<Handler<AsyncResult<ResultSet>>>() {
        @Override
        public boolean matches(Object o) {
          ((Handler<AsyncResult<ResultSet>>) o).handle(queryResult);
          return true;
        }
      }))).thenReturn(conn);

  }

  @Test
  public void fetchEntry_failureResult_ifGetConnectionFails() {
    when(connResult.succeeded()).thenReturn(false);
    final Throwable error = new Throwable();
    when(connResult.cause()).thenReturn(error);

    final Future<DictEntry> result = mock(Future.class);
    dao.fetchEntry(WORD, result);

    verify(result).handle(argThat(new ArgumentMatcher<AsyncResult<DictEntry>>() {

      @Override
      public boolean matches(Object o) {
        assertFalse(((AsyncResult) o).succeeded());
        assertEquals(error, ((AsyncResult) o).cause());
        return true;
      }
    }));
  }

  @Test
  public void fetchEntry_failureResult_ifQueryFails() {
    when(queryResult.succeeded()).thenReturn(false);
    final Throwable error = new Throwable();
    when(queryResult.cause()).thenReturn(error);

    final Future<DictEntry> result = mock(Future.class);
    dao.fetchEntry(WORD, result);

    verify(result).handle(argThat(new ArgumentMatcher<AsyncResult<DictEntry>>() {

      @Override
      public boolean matches(Object o) {
        assertFalse(((AsyncResult) o).succeeded());
        assertEquals(error, ((AsyncResult) o).cause());
        return true;
      }
    }));
  }


  @Test
  public void fetchEntry_fetchesDictEntry() {
    final Future<DictEntry> result = mock(Future.class);
    dao.fetchEntry(WORD, result);

    verify(result).handle(argThat(new ArgumentMatcher<AsyncResult<DictEntry>>() {
      @Override
      public boolean matches(Object o) {
        final DictEntry dictEntry = (DictEntry) ((AsyncResult) o).result();
        assertEquals(WORD, dictEntry.getWord());
        assertEquals(TRANSLATION, dictEntry.getClassMap().get(CLASS).get(0).getTranslation());
        assertEquals(COMMENT, dictEntry.getClassMap().get(CLASS).get(0).getComment());
        return true;
      }
    }));
  }

}