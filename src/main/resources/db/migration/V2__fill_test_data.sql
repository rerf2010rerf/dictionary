insert into word_classes (class_id, class_name) values (1, 'сущ');
insert into word_classes (class_id, class_name) values (2, 'гл');

insert into source_words (word_id, word, word_class) values (1, 'word', 1);

insert into target_words (word_id, word, word_class) values (1, 'слово', 1);
insert into target_words (word_id, word, word_class) values (2, 'слова', 1);
insert into target_words (word_id, word, word_class) values (3, 'выражать словами', 2);

insert into translates (word_id, translate_id, translate_comment) values (1, 1, null);
insert into translates (word_id, translate_id, translate_comment) values (1, 2, 'песни');
insert into translates (word_id, translate_id, translate_comment) values (1, 3, null);