create table word_classes (
	class_id bigserial primary key,
    class_name varchar(32)
);

create table source_words (
	word_id bigserial primary key,
    word varchar(128),
    word_class bigint references word_classes (class_id)
);

create table target_words (
	word_id bigserial primary key,
    word varchar(128),
    word_class bigint references word_classes (class_id)
);

create table translates (
	word_id bigint references source_words (word_id),
    translate_id bigint references target_words (word_id),
    translate_comment varchar(256)
);


select cl.class_name as class_name, t.word as translation_word, tr.translate_comment as comment_word
from word_classes cl, source_words s, target_words t, translates tr
where s.word = '?'
	and s.word_id = tr.word_id
	and tr.translate_id = t.word_id
	and t.word_class = cl.class_id;
