<html>
<head>
    <meta charset="utf-8">
    <link href="/css/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
<br>
<p class="lead">Перевод: <strong>${context.word}</strong></p>
<br>
<#list context.word_classes as entry>
    <dl class="dl-horizontal">
        <dt>${entry.key}</dt>
        <dd>
            <#list entry.value as trans_item>${trans_item.translation}<#if trans_item.comment??>(${trans_item.comment});<#else>;</#if>
            </#list>
        </dd>
    </dl>
    <#else>
        <p>Перевод не найден</p>
</#list>
</div>
</body>
</html>