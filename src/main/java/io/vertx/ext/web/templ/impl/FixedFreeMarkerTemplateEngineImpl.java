package io.vertx.ext.web.templ.impl;


import freemarker.cache.NullCacheStorage;
import freemarker.template.Configuration;
import freemarker.template.Template;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.FreeMarkerTemplateEngine;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Имплементация FreeMarkerTemplateEngine с исправлением бага с кодировкой страниц при запуске приложения
 * из консоли windows. Подробности здесь: https://github.com/vert-x3/vertx-web/issues/480#issuecomment-265154317.
 * Пока это, видимо, единственный способ это исправить.
 */
@Slf4j
public class FixedFreeMarkerTemplateEngineImpl extends CachingTemplateEngine<Template> implements FreeMarkerTemplateEngine {
  private final Configuration config;
  private final FreeMarkerTemplateLoader loader = new FreeMarkerTemplateLoader();

  public FixedFreeMarkerTemplateEngineImpl() {
    super("ftl", 10000);
    this.config = new Configuration(Configuration.VERSION_2_3_23);
    this.config.setObjectWrapper(new VertxWebObjectWrapper(this.config.getIncompatibleImprovements()));
    this.config.setTemplateLoader(this.loader);
    this.config.setCacheStorage(new NullCacheStorage());
  }

  public FreeMarkerTemplateEngine setExtension(String extension) {
    this.doSetExtension(extension);
    return this;
  }

  public FreeMarkerTemplateEngine setMaxCacheSize(int maxCacheSize) {
    this.cache.setMaxSize(maxCacheSize);
    return this;
  }

  public void render(RoutingContext context, String templateDirectory, String templateFileName, Handler<AsyncResult<Buffer>> handler) {
    try {
      templateFileName = templateDirectory + templateFileName;
      Template template = this.isCachingEnabled()?(Template)this.cache.get(templateFileName):null;
      if(template == null) {
        synchronized(this) {
          this.loader.setVertx(context.vertx());
          template = this.config.getTemplate(this.adjustLocation(templateFileName));
        }

        if(this.isCachingEnabled()) {
          this.cache.put(templateFileName, template);
        }
      }

      Map<String, RoutingContext> variables = new HashMap(1);
      variables.put("context", context);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      Throwable var8 = null;

      try {
        StringWriter result = new StringWriter();
        template.process(variables, result);
        handler.handle(Future.succeededFuture(Buffer.buffer(result.toString())));
      } catch (Throwable var19) {
        var8 = var19;
        throw var19;
      } finally {
        if(baos != null) {
          if(var8 != null) {
            try {
              baos.close();
            } catch (Throwable var18) {
              var8.addSuppressed(var18);
            }
          } else {
            baos.close();
          }
        }

      }
    } catch (Exception var22) {
      handler.handle(Future.failedFuture(var22));
    }

  }
}
