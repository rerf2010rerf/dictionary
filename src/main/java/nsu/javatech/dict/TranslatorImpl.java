package nsu.javatech.dict;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;


/**
 * Имплементация Translator для получения переводов из БД
 */
public class TranslatorImpl implements Translator {

  private static final String DATABASE_CONFIG_PARAM = "database";

  private final DictDao dao;

  /**
   * @param vertx - инстанс vertx
   * @param config - конфигурация, должен содержать JsonObject database с параметрами соединения с БД
   */
  public TranslatorImpl(final Vertx vertx, final JsonObject config) {
    this.dao = new DictDao(vertx, config.getJsonObject(DATABASE_CONFIG_PARAM));
  }

  @Override
  public void translate(final String word, final Handler<AsyncResult<DictEntry>> result) {
    dao.fetchEntry(word, result);
  }
}
