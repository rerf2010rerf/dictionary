package nsu.javatech.dict;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Словарная статья
 */
@Data
public class DictEntry {
  /**
   * Оригинальное слово, перевод которого представляет данная словарная статья
   */
  private String word;

  /**
   * Словарные категории переводов
   */
  private Map<String, List<TransItem>> classMap = new HashMap<>();


  /**
   * Элементы словарной статьи, представляют один конкретный перевод слова
   */
  @Data
  public static final class TransItem {
    /**
     * Перевод
     */
    private String translation;
    /**
     * Комментарий к переводу
     */
    private String comment;
  }
}
