package nsu.javatech.dict;


import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {

  public static void main(String[] args) {
    final Vertx vertx = Vertx.vertx();

    final ConfigStoreOptions store = new ConfigStoreOptions()
      .setType("file")
      .setFormat("yaml")
      .setConfig(new JsonObject()
        .put("path", "config.yaml")
      );

    final ConfigRetriever retriever = ConfigRetriever.create(vertx,
      new ConfigRetrieverOptions().addStore(store));

    retriever.getConfig(confResult -> {
      if (confResult.succeeded()) {
        log.trace("Config loaded, start deploy verticle");
        vertx.deployVerticle(new DictServer(), new DeploymentOptions().setConfig(confResult.result()));
      } else {
        log.error("Error config retrieving", confResult.cause());
      }
    });
  }
}
