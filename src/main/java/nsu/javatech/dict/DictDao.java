package nsu.javatech.dict;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rx.java.RxHelper;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

/**
 * dao для доступа к БД переводов
 */
@Slf4j
public class DictDao {

  private static final String TRANSLATION_WORD_COLUMN = "translation_word";
  private static final String COMMENT_WORD_COLUMN = "comment_word";
  private static final String CLASS_NAME_COLUMN = "class_name";

  private static final String SELECT_SQL =
    "select cl.class_name as " + CLASS_NAME_COLUMN +
      ", t.word as " + TRANSLATION_WORD_COLUMN +
      ", tr.translate_comment as " + COMMENT_WORD_COLUMN +
      " from word_classes cl, source_words s, target_words t, translates tr" +
      " where s.word = ?" +
      " and s.word_id = tr.word_id" +
      " and tr.translate_id = t.word_id" +
      " and t.word_class = cl.class_id";

  private final JDBCClient client;

  public DictDao(final Vertx vertx, final JsonObject config) {
    this.client = JDBCClient.createShared(vertx,
      config.copy().put("driver_class", "org.postgresql.Driver"));
  }

  /**
   * получить словарную статью для заданного слова
   * @param word - слово для перевода
   * @param resultHandler - словарная статья с результатами перевода
   */
  public void fetchEntry(final String word, final Handler<AsyncResult<DictEntry>> resultHandler) {
    final ObservableFuture<SQLConnection> connFuture = RxHelper.observableFuture();
    client.getConnection(connFuture.toHandler());
    connFuture.flatMap(conn -> {
      final ObservableFuture<ResultSet> queryFuture = RxHelper.observableFuture();
      conn.queryWithParams(SELECT_SQL, new JsonArray().add(word), queryResult -> {
        conn.close();
        queryFuture.toHandler().handle(queryResult);
      });
      return queryFuture;
    })
      .subscribe(
        resultSet -> resultHandler.handle(Future.succeededFuture(formDictEntry(word, resultSet))),
        error -> {
          log.error("Error entry fetching", error);
          resultHandler.handle(Future.failedFuture(error));
        }
      );
  }

  private DictEntry formDictEntry(final String word, final ResultSet resultSet) {
    final DictEntry entry = new DictEntry();
    entry.setWord(word);

    resultSet.getRows().forEach(row -> {
      final DictEntry.TransItem item = new DictEntry.TransItem();
      item.setTranslation(row.getString(TRANSLATION_WORD_COLUMN));
      item.setComment(row.getString(COMMENT_WORD_COLUMN));
      final String className = row.getString(CLASS_NAME_COLUMN);

      entry.getClassMap().putIfAbsent(className, new ArrayList<>());
      entry.getClassMap().get(className).add(item);
    });

    return entry;
  }


}
