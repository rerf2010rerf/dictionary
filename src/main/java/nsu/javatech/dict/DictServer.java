package nsu.javatech.dict;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.templ.FreeMarkerTemplateEngine;
import io.vertx.ext.web.templ.impl.FixedFreeMarkerTemplateEngineImpl;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rx.java.RxHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DictServer extends AbstractVerticle {

  private static final String SERVER_PORT_CONFIG_PARAM = "server-port";

  private static final String WORD_PARAM = "word";
  private static final String WORD_CLASSES_PARAM = "word_classes";
  private static final String TRANSLATE_FTL_PATH = "/translate.ftl";
  private static final String INDEX_FTL_PATH = "/index.ftl";
  private static final String TEMPLATES_FOLDER_PATH = "templates";
  private static final String MAIN_PATH = "/";
  private static final String TRANSLATE_PATH = "/translate";
  private static final String SERVER_HOST = "server-host";
  private static final String WORD_TO_TRANSLATE_PARAM = "word";


  private final FreeMarkerTemplateEngine templateEngine = new FixedFreeMarkerTemplateEngineImpl();
  private Translator translator;

  @Override
  public void start(final Future<Void> result) {
    translator = new TranslatorImpl(vertx, config());

    final HttpServer server = vertx.createHttpServer();
    final Router router = Router.router(vertx);

    router.route("/css/*").handler(StaticHandler.create());
    router.route(MAIN_PATH).handler(this::mainPageHandler);
    router.route(TRANSLATE_PATH).handler(this::translateHandler);

    server
      .requestHandler(router::accept)
      .listen(config().getInteger(SERVER_PORT_CONFIG_PARAM),
        config().getString(SERVER_HOST),
        ar -> {
          if (ar.succeeded()) {
            log.trace("Http server started");
            result.complete();
          } else {
            log.error("Error starting http server", ar.cause());
            result.fail(ar.cause());
          }
        });
  }

  private void translateHandler(final RoutingContext routingContext) {
    logRequest(routingContext);

    final ObservableFuture<DictEntry> transResult = RxHelper.observableFuture();
    translator.translate(routingContext.request().getParam(WORD_TO_TRANSLATE_PARAM), transResult.toHandler());
    transResult.flatMap(dictEntry -> {
      final ObservableFuture<Buffer> renderResult = RxHelper.observableFuture();
      fillTranslationData(routingContext, dictEntry);
      templateEngine.render(routingContext, TEMPLATES_FOLDER_PATH, TRANSLATE_FTL_PATH, renderResult.toHandler());
      return renderResult;
    })
      .subscribe(
        buffer -> {
          prepareRequest(routingContext);
          routingContext.response().end(buffer);
        },
        error -> {
          log.error("Error translate response", error);
          routingContext.fail(error);
        });
  }

  private void mainPageHandler(final RoutingContext routingContext) {
    logRequest(routingContext);

    templateEngine.render(routingContext, TEMPLATES_FOLDER_PATH, INDEX_FTL_PATH, ar -> {
      if (ar.succeeded()) {
        prepareRequest(routingContext);
        routingContext.response().end(ar.result());
      } else {
        log.error("Error main page response", ar.cause());
        routingContext.fail(ar.cause());
      }
    });
  }

  private void fillTranslationData(final RoutingContext context, final DictEntry dictEntry) {
    context.put(WORD_PARAM, dictEntry.getWord());
    context.put(WORD_CLASSES_PARAM, dictEntry.getClassMap().entrySet());
  }

  private void prepareRequest(final RoutingContext routingContext) {
    routingContext.response().putHeader("Content-Type", "text/html");
  }

  private void logRequest(final RoutingContext routingContext) {
    final HttpServerRequest req = routingContext.request();
    log.trace("Request: URI = {}, params = {}", req.absoluteURI(), req.params());
  }
}
