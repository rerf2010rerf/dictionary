package nsu.javatech.dict;


import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

/**
 * Интерфейс переводчика слов
 */
public interface Translator {

  /**
   * Возвращает словарную статью заданного слова
   * @param word - слово для перевода
   * @param result - handler для DictEntry - результат перевода
   */
  void translate(String word, Handler<AsyncResult<DictEntry>> result);
}
